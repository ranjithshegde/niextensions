# Kinect features

A simple example project that demonstratues the parameter coupling ideas
mentioned in my thesis in an exceedingly simple way.

This project uses the body tracker algorithm, which is essentially a data
structure that contains technical descriptions of body identification from MRI
and other medical scanning appliances. The body form recognized by the kinect is
queried against this database to establish the knoweldge of joints, limbs and
head.

```cpp
   ofxNiTE2::User::Ref user = tracker.getUser(i);
   const ofxNiTE2::Joint& head = user->getJoint(nite::JOINT_HEAD);
   const ofxNiTE2::Joint& RHD = user->getJoint(nite::JOINT_RIGHT_HAND);
   const ofxNiTE2::Joint& LHD = user->getJoint(nite::JOINT_LEFT_HAND);
   const ofxNiTE2::Joint& LKN = user->getJoint(nite::JOINT_LEFT_KNEE);
   const ofxNiTE2::Joint& RKN = user->getJoint(nite::JOINT_RIGHT_KNEE);
   const ofxNiTE2::Joint& REL = user->getJoint(nite::JOINT_RIGHT_ELBOW);
   const ofxNiTE2::Joint& LEL = user->getJoint(nite::JOINT_LEFT_ELBOW);
```

Three new composite co-ordinates are formed based on certain relationships
identified or implied by the body movements.

1. `rh` is the distance vector between the right hand and the head
2. `lb` is the distance vector between left elbow and the head
3. `norm` is the normalized cross product of right hand and left elbow

One cricle is drawn using `norm` as its co-ordinates: Essentially making the
relationship between the distance of the head from both hands and their cross
product the controller of this value.

Another circle is drawn using the right hand as the co-ordinate and the angle
between right hand & left elbow as the radious of the circle.

This should serve as a reference on finding new relationships (ideally based on
the tendencies of the movement artist) and enforcing new combination of these
relationships to create new control values that are otherwise not possible to
find/imagine.
