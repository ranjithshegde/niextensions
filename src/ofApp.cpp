#include "ofApp.h"
#include "ofGraphics.h"
#include "quaternion.hpp"
#include "quaternion_geometric.hpp"
#include "vector_angle.hpp"

ofEasyCam cam;

//--------------------------------------------------------------
void ofApp::setup()
{
    ofSetFrameRate(60);
    ofSetVerticalSync(true);
    ofBackground(0);

    device.setLogLevel(OF_LOG_NOTICE);
    device.setup(0);
    tracker.setup(device);
    tracker.enableTrackingOutOfFrame(true);
}

void ofApp::exit()
{
    tracker.exit();
    device.exit();
}

//--------------------------------------------------------------
void ofApp::update()
{
    device.update();
}

//--------------------------------------------------------------
void ofApp::draw()
{
    depthPixels = tracker.getPixelsRef(1000, 4000);
    depthTexture.loadData(depthPixels);

    // draw in 2D
    ofSetColor(255);
    depthTexture.draw(0, 0);
    tracker.draw();

    // // draw 3D skeleton in 2D
    ofPushView();
    tracker.getOverlayCamera().begin(ofRectangle(0, 0, depthTexture.getWidth(), depthTexture.getHeight()));
    ofDrawAxis(100);
    tracker.draw3D();
    tracker.getOverlayCamera().end();
    ofPopView();
    // // draw in 3D

    // draw box
    ofNoFill();
    ofSetColor(255, 0, 0);
    for (int i = 0; i < tracker.getNumUser(); i++) {
        ofxNiTE2::User::Ref user = tracker.getUser(i);
        const ofxNiTE2::Joint& head = user->getJoint(nite::JOINT_HEAD);
        const ofxNiTE2::Joint& RHD = user->getJoint(nite::JOINT_RIGHT_HAND);
        const ofxNiTE2::Joint& LHD = user->getJoint(nite::JOINT_LEFT_HAND);
        const ofxNiTE2::Joint& LKN = user->getJoint(nite::JOINT_LEFT_KNEE);
        const ofxNiTE2::Joint& RKN = user->getJoint(nite::JOINT_RIGHT_KNEE);
        const ofxNiTE2::Joint& REL = user->getJoint(nite::JOINT_RIGHT_ELBOW);
        const ofxNiTE2::Joint& LEL = user->getJoint(nite::JOINT_LEFT_ELBOW);

        rightHandPos = RHD.getGlobalPosition();
        // rightHandPos = RHD.getPosition();
        leftHandPos = LHD.getGlobalPosition();
        lbow = REL.getGlobalPosition();
        // lbow = REL.getPosition();

        cross = glm::cross(glm::normalize(rightHandPos), glm::normalize(lbow));

        norm = glm::normalize(cross);
        hPos = head.getGlobalPosition();
        // hPos = head.getPosition();

        ofVec3f rH = (rightHandPos - hPos);
        ofVec3f lB = lbow - hPos;

        auto angle = rH.angle(lB);
        // auto angle = glm::angle(rightHandPos - hPos, lbow - hPos);

        cout << angle << " rH: " << rH << "  lB: " << lB << "\n";

        head.transformGL();
        ofDrawBox(10);
        head.restoreTransformGL();

        // cout << norm << endl;
        ofFill();
        ofSetColor(20, 249, 68);
        norm.x = ofMap(norm.x, -1.0, 1.0, 0, ofGetWidth(), true);
        norm.y = ofMap(norm.y, -1.0, 1.0, 0, ofGetHeight(), true);

        rH.x = ofMap(rH.x, -300, 300, 0, ofGetWidth(), true);
        rH.y = ofMap(rH.y, -150, 300, 0, ofGetHeight(), true);
        ofDrawCircle(norm.x, norm.y, 50);

        lB.x = ofMap(lB.x, -300, 300, 0, ofGetWidth(), true);
        lB.y = ofMap(lB.y, -150, 300, 0, ofGetHeight(), true);
        ofSetColor(ofColor::blue);
        ofDrawCircle(rH, angle);
        ofSetColor(ofColor::beige);
        ofDrawCircle(lB, 1 / angle * TWO_PI + ofRandom(20));
    }

    // cam.end();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key)
{
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key)
{
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y)
{
}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button)
{
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button)
{
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button)
{
}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h)
{
}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg)
{
}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo)
{
}
